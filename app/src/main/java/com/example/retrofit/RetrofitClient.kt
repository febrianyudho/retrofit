package com.example.retrofit

import retrofit2.Retrofit

object RetrofitClient {
    private const val BASE_URL= "httpz://jsonplaceholder.typicode.com"

    val instances: Api by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        retrofit.create(Api::class.java)
    }
}