package com.example.retrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private  val list = ArrayList<PostRespons>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        rvPost.setHasFixedSize(true)
        rvPost.layoutManager= linearlayoutmanager( this)

        RetrofitClient.instances.getPosts().enqueue(object: Handler.Callback<ArrayList<PostRespons>>,
            Callback<ArrayList<PostRespons>> {
            override fun onResponse(
                call: Call<ArrayList<PostRespons>>,
                response: Response<ArrayList<PostRespons>>
            ) {
                val responseCode = response.code().toString()
                tvResponCode.text = responseCode
                response.body()?.let { list.addAll(it) }
                val adapter= PostAdapter(list)
                rvPost.adapter =adapter
            }

            override fun onFailure(call: Call<ArrayList<PostRespons>>, t: Throwable) {
                TODO("Not yet implemented")
            }

        })

    }
}