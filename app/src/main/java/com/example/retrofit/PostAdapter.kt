package com.example.retrofit

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class PostAdapter(private  val list: ArrayList<PostRespons>):RecyclerView.Adapter<PostAdapter.PostViewHolder>(){
    inner class  PostViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(postResponse : PostRespons){
            with(itemView){
                val text = "id : ${postResponse.id}\n" +
                        "title : ${postResponse.title}\n"+
                        "text :  ${postResponse.text}"
                tvText.text =text
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
       val view =  LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent. false)
         return PostViewHolder()
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size


}